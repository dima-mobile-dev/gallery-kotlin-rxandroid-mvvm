package com.andromeda.photoalbum

import android.app.Application
import com.andromeda.photoalbum.di.component.BaseComponent
import com.andromeda.photoalbum.di.component.DaggerBaseComponent
import com.andromeda.photoalbum.di.module.AlbumModule
import com.andromeda.photoalbum.di.module.AppModule
import com.andromeda.photoalbum.di.module.DataModule
import com.andromeda.photoalbum.di.module.NetworkModule

class AlbumApplication : Application() {

    private lateinit var baseComponent: BaseComponent

    override fun onCreate() {
        super.onCreate()
        initDependencies()
    }

    private fun initDependencies() {
        baseComponent = DaggerBaseComponent.builder()
            .appModule(AppModule(applicationContext))
            .dataModule(DataModule())
            .networkModule(NetworkModule())
            .build()
    }

    fun getBaseComponent() = baseComponent

    fun createAlbumComponent() = baseComponent.plus(AlbumModule())
}