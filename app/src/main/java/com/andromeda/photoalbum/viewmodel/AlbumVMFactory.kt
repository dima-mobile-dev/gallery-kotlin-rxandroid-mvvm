package com.andromeda.photoalbum.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.andromeda.photoalbum.usecase.GetPhotosUseCase

class AlbumVMFactory(private val getPhotosUseCase: GetPhotosUseCase) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return AlbumViewModel(getPhotosUseCase) as T
    }
}