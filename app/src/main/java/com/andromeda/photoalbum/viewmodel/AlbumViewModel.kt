package com.andromeda.photoalbum.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.paging.PagedList
import com.andromeda.photoalbum.base.BaseViewModel
import com.andromeda.photoalbum.model.Photo
import com.andromeda.photoalbum.usecase.Failure
import com.andromeda.photoalbum.usecase.GetPhotosUseCase
import com.andromeda.photoalbum.usecase.Success
import com.andromeda.photoalbum.util.extension.exhaustive
import com.andromeda.photoalbum.view.AlbumViewState

class AlbumViewModel(private val getPhotosUseCase: GetPhotosUseCase) : BaseViewModel() {

    private val mViewState: MutableLiveData<AlbumViewState> = MutableLiveData()
    val viewState: LiveData<AlbumViewState> = mViewState

    init {
        mViewState.value = AlbumViewState()
    }

    fun loadPhotos(refresh: Boolean = false) {
        mViewState.value = viewState.value?.copy(loading = true)
        getPhotosUseCase.execute(GetPhotosUseCase.Input(refresh))
            .subscribe {
                when (it) {
                    is Success -> handleGetPhotosSuccess(it.data)
                    is Failure -> handleFailure(it.error)
                }.exhaustive
            }
            .autoClear()
    }

    private fun handleGetPhotosSuccess(photoList: PagedList<Photo>) {
        mViewState.value = viewState.value?.copy(
            loading = false,
            photoList = photoList,
            error = ""
        )
    }

    private fun handleFailure(error: String) {
        mViewState.value = viewState.value?.copy(
            loading = false,
            error = error
        )
    }
}