package com.andromeda.photoalbum.di.module

import android.content.Context
import com.andromeda.photoalbum.model.api.PhotoApi
import com.andromeda.photoalbum.model.db.AppDatabase
import com.andromeda.photoalbum.model.db.PhotoDao
import com.andromeda.photoalbum.model.repo.PhotoRepository
import com.andromeda.photoalbum.util.mapper.PhotoApiToDbMapper
import com.andromeda.photoalbum.util.mapper.PhotoDataToPhotoMapper
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DataModule {

    @Provides
    @Singleton
    fun provideRoomDatabase(context: Context) = AppDatabase.getInstance(context)

    @Provides
    @Singleton
    fun providePhotoDao(database: AppDatabase) = database.photoDao()

    @Provides
    @Singleton
    fun providePhotoRepository(
        photoDao: PhotoDao,
        photoApi: PhotoApi,
        photoDataToPhotoMapper: PhotoDataToPhotoMapper,
        photoApiToDbMapper: PhotoApiToDbMapper
    ): PhotoRepository {
        return PhotoRepository(photoDao, photoApi, photoDataToPhotoMapper, photoApiToDbMapper)
    }
}