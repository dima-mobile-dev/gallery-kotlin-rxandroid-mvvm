package com.andromeda.photoalbum.di.component

import com.andromeda.photoalbum.di.module.AlbumModule
import com.andromeda.photoalbum.di.scope.PerActivity
import com.andromeda.photoalbum.view.AlbumActivity
import dagger.Subcomponent

@PerActivity
@Subcomponent(modules = [AlbumModule::class])
interface AlbumSubComponent {

    fun inject(albumActivity: AlbumActivity)
}