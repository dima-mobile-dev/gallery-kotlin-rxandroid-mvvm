package com.andromeda.photoalbum.di.module

import com.andromeda.photoalbum.model.repo.PhotoRepository
import com.andromeda.photoalbum.usecase.GetPhotosUseCase
import com.andromeda.photoalbum.util.rx.AsyncTransformer
import com.andromeda.photoalbum.viewmodel.AlbumVMFactory
import dagger.Module
import dagger.Provides

@Module
class AlbumModule {

    @Provides
    fun provideAlbumVMFactory(getPhotosUseCase: GetPhotosUseCase) = AlbumVMFactory(getPhotosUseCase)

    @Provides
    fun provideGetPhotosUseCase(photoRepository: PhotoRepository) =
        GetPhotosUseCase(photoRepository, AsyncTransformer())
}