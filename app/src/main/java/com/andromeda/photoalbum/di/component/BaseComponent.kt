package com.andromeda.photoalbum.di.component

import com.andromeda.photoalbum.di.module.AlbumModule
import com.andromeda.photoalbum.di.module.AppModule
import com.andromeda.photoalbum.di.module.DataModule
import com.andromeda.photoalbum.di.module.NetworkModule
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AppModule::class,
        DataModule::class,
        NetworkModule::class
    ]
)
interface BaseComponent {

    fun plus(albumModule: AlbumModule): AlbumSubComponent
}