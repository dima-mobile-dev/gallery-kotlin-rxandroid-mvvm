package com.andromeda.photoalbum.model

data class Photo(
    var id: String,
    var width: Int,
    var height: Int,
    var fullUrl: String,
    var thumbUrl: String
)