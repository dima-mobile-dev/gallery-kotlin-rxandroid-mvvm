package com.andromeda.photoalbum.model.db

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "photos")
data class PhotoData(

    @PrimaryKey
    val id: String,

    val width: Int,

    val height: Int,

    @ColumnInfo(name = "full_url")
    val fullUrl: String,

    @ColumnInfo(name = "thumb_url")
    val thumbUrl: String
)