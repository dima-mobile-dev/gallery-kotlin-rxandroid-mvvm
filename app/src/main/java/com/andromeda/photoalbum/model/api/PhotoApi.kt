package com.andromeda.photoalbum.model.api

import com.andromeda.photoalbum.model.db.AppDatabase
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface PhotoApi {

    @GET("photos")
    fun getPhotos(
        @Query("page") page: Int?,
        @Query("per_page") perPage: Int? = AppDatabase.PAGE_SIZE
    ): Single<List<PhotoApiData>>
}