package com.andromeda.photoalbum.model.api

import com.google.gson.annotations.SerializedName

data class Urls(

    @SerializedName("regular")
    var fullUrl: String,

    @SerializedName("thumb")
    var thumbUrl: String
)