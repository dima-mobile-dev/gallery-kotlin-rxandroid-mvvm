package com.andromeda.photoalbum.model.api

data class PhotoApiData(
    var id: String,
    var width: Int,
    var height: Int,
    var urls: Urls
)