package com.andromeda.photoalbum.model.repo

import androidx.paging.PagedList
import androidx.paging.RxPagedListBuilder
import com.andromeda.photoalbum.model.Photo
import com.andromeda.photoalbum.model.api.PhotoApi
import com.andromeda.photoalbum.model.api.PhotoApiData
import com.andromeda.photoalbum.model.db.AppDatabase
import com.andromeda.photoalbum.model.db.PhotoDao
import com.andromeda.photoalbum.model.db.PhotoData
import com.andromeda.photoalbum.usecase.Result
import com.andromeda.photoalbum.util.mapper.Mapper
import io.reactivex.Observable

class PhotoRepository(
    private val photoDao: PhotoDao,
    private val photoApi: PhotoApi,
    private val photoDataToPhotoMapper: Mapper<PhotoData, Photo>,
    private val photoApiToDbMapper: Mapper<PhotoApiData, PhotoData>
) {

    /**
     * Get all photos in the database (lazy loading)
     * @return a [Result] of all photos with lazy loading supported by wrapping in a [PagedList]
     */
    fun getPhotos(refresh: Boolean): Observable<Result<PagedList<Photo>>> {
        val dataSourceFactory = photoDao.getPhotos()
            .map(photoDataToPhotoMapper::mapFrom)

        val pagingConfig = PagedList.Config.Builder()
            .setPageSize(AppDatabase.PAGE_SIZE)
            .setPrefetchDistance(AppDatabase.PREFETCH_DISTANCE)
            .setInitialLoadSizeHint(AppDatabase.INITIAL_LOAD_SIZE)
            .setEnablePlaceholders(false)
            .build()

        val boundaryCallback = PhotoBoundaryCallback(refresh, photoDao, photoApi, photoApiToDbMapper)

        val data = RxPagedListBuilder(dataSourceFactory, pagingConfig)
            .setBoundaryCallback(boundaryCallback)
            .buildObservable()
            .map {
                Result.success(it)
            }
            .doOnSubscribe {
                if (refresh) boundaryCallback.requestAndSaveData()
            }

        return Observable.merge(data, boundaryCallback.errors)
    }
}