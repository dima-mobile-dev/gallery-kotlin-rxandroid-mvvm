package com.andromeda.photoalbum.util.rx

import io.reactivex.ObservableTransformer

abstract class Transformer<T> : ObservableTransformer<T, T>