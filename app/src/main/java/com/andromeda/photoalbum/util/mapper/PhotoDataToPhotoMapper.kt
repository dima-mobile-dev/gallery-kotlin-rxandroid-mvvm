package com.andromeda.photoalbum.util.mapper

import com.andromeda.photoalbum.model.Photo
import com.andromeda.photoalbum.model.db.PhotoData
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class PhotoDataToPhotoMapper @Inject constructor() : Mapper<PhotoData, Photo> {

    override fun mapFrom(from: PhotoData): Photo {
        return Photo(
            id = from.id,
            width = from.width,
            height = from.height,
            fullUrl = from.fullUrl,
            thumbUrl = from.thumbUrl
        )
    }
}