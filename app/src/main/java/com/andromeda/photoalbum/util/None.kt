package com.andromeda.photoalbum.util

class None {

    override fun equals(other: Any?): Boolean {
        return true
    }

    override fun hashCode(): Int {
        return javaClass.hashCode()
    }
}