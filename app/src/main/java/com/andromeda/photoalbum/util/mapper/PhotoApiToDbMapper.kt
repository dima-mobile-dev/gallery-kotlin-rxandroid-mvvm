package com.andromeda.photoalbum.util.mapper

import com.andromeda.photoalbum.model.api.PhotoApiData
import com.andromeda.photoalbum.model.db.PhotoData
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class PhotoApiToDbMapper @Inject constructor() : Mapper<PhotoApiData, PhotoData> {

    override fun mapFrom(from: PhotoApiData): PhotoData {
        return PhotoData(
            id = from.id,
            width = from.width,
            height = from.height,
            fullUrl = from.urls.fullUrl,
            thumbUrl = from.urls.thumbUrl
        )
    }
}