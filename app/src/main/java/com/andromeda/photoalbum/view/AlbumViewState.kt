package com.andromeda.photoalbum.view

import androidx.paging.PagedList
import com.andromeda.photoalbum.model.Photo

data class AlbumViewState(
    var loading: Boolean = false,
    var error: String = "",
    var photoList: PagedList<Photo>? = null
)