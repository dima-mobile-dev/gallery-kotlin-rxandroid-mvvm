package com.andromeda.photoalbum.usecase

import androidx.paging.PagedList
import com.andromeda.photoalbum.model.Photo
import com.andromeda.photoalbum.model.repo.PhotoRepository
import com.andromeda.photoalbum.util.rx.Transformer
import io.reactivex.Observable

class GetPhotosUseCase(
    private val photoRepository: PhotoRepository,
    transformer: Transformer<Result<PagedList<Photo>>>? = null
) : UseCase<GetPhotosUseCase.Input, PagedList<Photo>>(transformer) {

    override fun buildObservable(input: Input): Observable<Result<PagedList<Photo>>> {
        return photoRepository.getPhotos(input.refresh)
    }

    data class Input(val refresh: Boolean = false)
}